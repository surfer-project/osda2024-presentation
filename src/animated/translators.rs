@h1[
pub trait Translator {
  @h2[fn variable_info(&self, variable: &VariableMeta)
    -> Result<VariableInfo>;@]
  @h3[fn translate(&self, variable, value)
    -> Result<TranslationResult>;@]
  @h4[fn translates(&self, variable: &VariableMeta)
    -> Result<TranslationPreference>;@]
}@]
@@lang=rustp

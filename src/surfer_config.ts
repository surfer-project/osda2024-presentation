
export const CONFIG = `
default_variable_name_type = "Unique"
default_clock_highlight_type = "Line"
snap_distance = 6

[default_time_format]
format = "No"
show_space = true
show_unit = true

[layout]
show_hierarchy = true
show_menu = true
show_toolbar = true
show_ticks = true
show_tooltip = true
show_overview = true
show_statusbar = true
show_variable_indices = true
window_width = 1920
window_height = 1080
align_names_right = false
hierarchy_style = "Separate"

[gesture]
style = { color="e9da16", width=2 }
size = 300
deadzone = 20

[behavior]
keep_during_reload = true
arrow_key_bindings = "Edge"

[ticks]
style = { color="222222", width=2 }
density = 1

[theme]
foreground = "d4d4d4"
alt_text_color = "2b2b2b" # negated foreground
border_color = "282b2d"
selected_elements_colors = { background = "444444", foreground = "d4d4d4" }
canvas_colors = { background = "000000", alt_background = "000000", foreground = "ffffff" }
primary_ui_color = { background = "000000", foreground = "ffffff" }
secondary_ui_color = { background = "000000", foreground = "ffffff" }
accent_info = { background = "7b9aff", foreground = "d4d4d4" }
accent_warn = { background = "32302f", foreground = "d4d4d4" }
accent_error = { background = "d2302f", foreground = "d4d4d4" }
clock_highlight_line = { color = "777777", width = 2 }
clock_highlight_cycle = "12222f"
variable_default = "54f30d"
variable_undef = "f44747"
variable_highimp = "c9e124"
variable_dontcare = "4040ff"
variable_weak = "808080"
cursor = { color="b63935", width=2 }
linewidth = 1.5
alt_frequency = 3
viewport_separator = { color="d4d4d4", width=4 }

[theme.colors]
Green = "6a9955"
Red = "f44747"
Yellow = "ffd602"
Blue = "569cd6"
Pink = "c586c0"
Orange = "ce9178"
Gray = "808080"
Violet = "6e0087"
`

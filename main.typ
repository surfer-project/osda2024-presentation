#import "typst-presentation-common/theme.typ": *
#import "typst-presentation-common/highlight.typ": *
#import "@preview/cades:0.3.0": qr-code

#set text(size: 25pt, font: "Noto Sans")

#show strong: set text(fill: rgb(75%, 100%, 80%))

#show: university-theme.with(
  progress-bar: false
)

#show: highlights

#let both_logos(height) = [#box(image("liu_logo.svg", height: height))]

// #show raw.where(lang: "spade"): it => [
//   #show regex("\b(entity|let|inst|end)\b") : keyword => text(weight:"bold", keyword)
//   #it
// ]

#slide(title: "")[
  #set align(center)
  #v(3cm)
  #set text(size: 40pt)
  *Extensibility*
]
#slide(title: "Extensibility")[
  Translators are easy to add

  #include("./animated_out/translators.typ")
]

#slide(title: "Extensibility")[
  Supports multiple waveform sources

  VCD, FST, GHW

  Or completely different formats
    - TLM transaction streams

  Interactively via TCP
]

#slide(title: "Integrated Analysis")[
  Analysis features inside Surfer with WAL
    - Via TCP
    - Long term goal: WAL directly inside Surfer 
  
  Performance Analysis

  Inject new logic inside Surfer
    - Create little helpers
    - ready && valid
    - `req |-> ##[1:2] ack` 
    - Create abstractions for buses
]

#slide(title: "Remote Control")[
  Remote Control Protocol via TCP, (Web)Sockets, ...

  Full control over the waveform viewer  
    - Add or remove signals
    - Zoom or pan around
    - Mark parts, draw inside Waveform

  Applications
    - WSVA, mark failing assertions, show sub expressions
    - Simulators 
  Not just for Surfer, intended for general use
    - Still WIP
]

#slide(title: "Performance")[
  #set align(center);
  #image("./fig/performance.png")
]

#let wasm_with_logo = stack(
  dir: ltr,
  [#v(-0.1em) #image("./fig/WebAssembly_Logo.svg", height: 1.0em)],
  h(0.5em),
  [Web Assembly!]
)

#slide(title: "Runs everywhere")[
  Linux, Mac and Windows of course.

  #wasm_with_logo
]

#slide(title: wasm_with_logo)[
  - No installation
  - Inspect waves in continuous integration
  - Embeddable in other things
    - VSCode?
]
#slide(title: "Embeddable")[
  #image("./fig/sonicrv.png")
]
#slide(title: "Embeddable")[
  #image("./fig/presentation.png")
]

#slide(title: "Some technical details")[
  #grid(columns: (50%, 50%), [
    Written in Rust

    #v(0.5cm)

    #only("1", [- Nearly Free Web Assembly support])
    #only("2", [- (Perhaps too) Easy dependency management])
    ],
    [
      #only(2, image("./fig/dependencies.png"))
    ]
  )
]

#slide(title: "Waveform Backends")[
  We don't do the wave parsing ourselves

  *FastWaveBackend* - Yehowshua Immanuel
]

#slide(title: "Waveform Backends")[
  We needed FST support. Kevin Laeufer asked me if I wanted a Rust library for it

  #uncover(2, [
  Enter *Wellen*

  - Multi-threaded loading
    - 2 minutes reduced to 6 seconds
  - On-demand decompression
    - Large VCDs use much less memory
  ])
]

#slide(title: "GUI Architecture")[
  Elm Architecture

  - Centralized State
  - All interactions via messages
  - Allows easy integration with new input methods and extensions
]

#slide(title: "Thanks!")[
  - *Yehowshua Immanuel* for writing FastWaveBackend
  - *Kevin Laeufer* for writing Wellen
  - Contributors
    - *Andreas Wallner*
    - *Matt Taylor*
    - *Francesco Urbani*
    - *Tom Verbeure*
    - *Hugo Lundin*
    - *Verneri Hirvonen*
    - *Paul Blume*
  Everyone who has provided feedback
]

#slide(title: "Conclusions and A call for action")[
  Surfer is a *snappy* and *extensible* waveform viewer that *runs everywhere*

  #uncover("2-", [What do *you* want from a waveform viewer?])

  #let both_logos = box(
    height: 2cm,
    stack(dir: ltr, [#image("./fig/liu_logo.svg")], [#image("./fig/JKU_Logo.svg")])
  )

  #uncover(3, [#stack(dir: ltr, stack(dir: ttb, [#v(3cm) #h(6cm) Try it on your phone!], v(2cm), both_logos), h(2cm), [
    #qr-code("osda24.surfer-project.org", width: 7cm)
  ])])
]

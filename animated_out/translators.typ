#import "../typst-presentation-common/theme.typ": *
#import "../typst-presentation-common/highlight.typ": *

#set par(leading: 0.5em)

#let unfocused_color = rgb(60%, 60%, 60%)

#only(1, [#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("pub trait Translator {", lang: "rustp"))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("fn variable_info(&self, variable: &VariableMeta)", lang: "rustp"))\
#box(raw("    -> Result<VariableInfo>;", lang: "rustp"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("fn translate(&self, variable, value)", lang: "rustp"))\
#box(raw("    -> Result<TranslationResult>;", lang: "rustp"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("fn translates(&self, variable: &VariableMeta)", lang: "rustp"))\
#box(raw("    -> Result<TranslationPreference>;", lang: "rustp"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("}", lang: "rustp"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(2, [#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("pub trait Translator {"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("fn variable_info(&self, variable: &VariableMeta)", lang: "rustp"))\
#box(raw("    -> Result<VariableInfo>;", lang: "rustp"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("fn translate(&self, variable, value)"), fill: unfocused_color))\
#box(text(raw("    -> Result<TranslationResult>;"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("fn translates(&self, variable: &VariableMeta)"), fill: unfocused_color))\
#box(text(raw("    -> Result<TranslationPreference>;"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(3, [#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("pub trait Translator {"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("fn variable_info(&self, variable: &VariableMeta)"), fill: unfocused_color))\
#box(text(raw("    -> Result<VariableInfo>;"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("fn translate(&self, variable, value)", lang: "rustp"))\
#box(raw("    -> Result<TranslationResult>;", lang: "rustp"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("fn translates(&self, variable: &VariableMeta)"), fill: unfocused_color))\
#box(text(raw("    -> Result<TranslationPreference>;"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(4, [#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("pub trait Translator {"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("fn variable_info(&self, variable: &VariableMeta)"), fill: unfocused_color))\
#box(text(raw("    -> Result<VariableInfo>;"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("fn translate(&self, variable, value)"), fill: unfocused_color))\
#box(text(raw("    -> Result<TranslationResult>;"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("fn translates(&self, variable: &VariableMeta)", lang: "rustp"))\
#box(raw("    -> Result<TranslationPreference>;", lang: "rustp"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])
